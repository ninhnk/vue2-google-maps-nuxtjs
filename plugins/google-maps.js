import Vue from 'vue'
import * as VueGoogleMaps from '~/node_modules/vue2-google-maps/src/main'

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAiMzxeGct80UAE1eJaA7vUaa4AdrxM9_g',
        libraries: 'places',
    },
})